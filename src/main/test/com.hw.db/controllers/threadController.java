package com.hw.db.controllers;


import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import static org.junit.jupiter.api.Assertions.assertEquals;

class threadControllerTests {
    private Thread thread;
    private String SLUG = "slug";
    private String userName = "vvertash";
    private int testId = 13;

    @BeforeEach
    @DisplayName("Initialization for tests")
    void createThreadTest() {
        thread = new Thread(userName, new Timestamp(0), "SQR Lab 3", "How to..", "slug", "How to..", 999);
        thread.setId(testId);
    }

    @Test
    @DisplayName("Check by id and slug")
    void testCheckIdOrSlug() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadById(testId)).thenReturn(thread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(thread, tc.CheckIdOrSlug(SLUG));
            assertEquals(thread, tc.CheckIdOrSlug(Integer.toString(testId)));
        }
    }

    @Test
    @DisplayName("Post Creation")
    void testCreatePost() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(posts), tc.createPost(SLUG, posts));
        }
    }

    @Test
    @DisplayName("Get Post by id and slug")
    void testGetPosts() {
        List<Post> posts = new ArrayList<>();
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getPosts(thread.getId(), 100, 1, "sort", true)).thenReturn(posts);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(posts), tc.Posts(SLUG, 100, 1, "sort", true));
        }
    }

    @Test
    @DisplayName("Change Post with new Thread")
    void testChange() {
        Thread changedThread = new Thread("tomato", new Timestamp(100), "tomato is  vegetable", "is tomato vegetable or fruit", "slug slug", "Please help to understand", 0);
        changedThread.setId(testId+1);

        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("slug slug")).thenReturn(changedThread);
            threadMock.when(() -> ThreadDAO.getThreadById(testId+1)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.change("slug slug", thread), "New Message in Thread");
        }
    }

    @Test
    @DisplayName("Vote for Thread")
    void testCreateVote() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try (MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                User user = new User(userName, "vvertash@gmail.com", "Valeria Vertash", "Tome is vegetable");
                Vote vote = new Vote(userName, 1);
                threadController tc = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);
                userMock.when(() -> UserDAO.Info(userName)).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread).getStatusCode(), tc.createVote(SLUG, vote).getStatusCode());
            }
        }
    }

    @Test
    @DisplayName("Get information about first thread")
    void testInfo() {
        try (MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController tc = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(SLUG)).thenReturn(thread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thread), tc.info(SLUG));
        }
    }


}